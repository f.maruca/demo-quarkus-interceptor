package demo.quarkus.interceptor.services;

import demo.quarkus.interceptor.exceptionMapper.ExceptionUtility;
import demo.quarkus.interceptor.json.JsonRequestHello;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;

@ApplicationScoped
public class Service1 {

    @Inject
    private Service2 Service2;

    public static final int CODE_LEVEL = 2;


    public Response getResponse(int exceptionLevel, JsonRequestHello jsonRequest) throws Exception {
        ExceptionUtility.isExceptionInThisCode(CODE_LEVEL, exceptionLevel);
        Response response = this.Service2.getResponse(exceptionLevel, jsonRequest);
        return response;
    }

    public Response getResponseBis(JsonRequestHello jsonRequest) throws Exception {
        try {
            Response response = this.Service2.getResponseBis(jsonRequest);
            return response;
        } catch (Exception e) {
            throw new RuntimeException("RuntimeException");
        }
    }



}
