package demo.quarkus.interceptor.json;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JsonResponseHello {

    @JsonProperty("nomeXXX")
    private String nome;
    private String cognome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }
}
