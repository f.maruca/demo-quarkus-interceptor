//package demo.quarkus.interceptor.exceptionMapper;
//
//import jakarta.ws.rs.core.Response;
//import jakarta.ws.rs.ext.ExceptionMapper;
//import jakarta.ws.rs.ext.Provider;
//
//@Provider
//public class CustomMultipleExceptionMapper implements ExceptionMapper<Exception> {
//
//
//    @Override
//    public Response toResponse(Exception exception) {
//        Response response = mapExceptionToResponse(exception);
//        return response;
//    }
//
//
//    private Response mapExceptionToResponse(Exception exception) {
//
//        if (exception instanceof CustomException) {
//            return Response.status(Response.Status.BAD_REQUEST).
//                    entity(new ErrorMessage(exception.getMessage(), false)).build();
//
//
//        }else if (exception instanceof RuntimeException) {
//            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).
//                    entity(new ErrorMessage(exception.getMessage(), false)).build();
//
//
//        }else if (exception instanceof Exception) {
//            return Response.status(Response.Status.NOT_ACCEPTABLE).
//                    entity(new ErrorMessage(exception.getMessage(), false)).build();
//
//        }else{
//            return Response.status(Response.Status.NOT_FOUND).
//                    entity(new ErrorMessage(exception.getMessage(), false)).build();
//        }
//
//    }
//
//
//}
