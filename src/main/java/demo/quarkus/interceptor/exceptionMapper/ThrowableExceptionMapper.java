package demo.quarkus.interceptor.exceptionMapper;//package demo.quarkus.interceptor.exceptionMapper;


import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ResourceBundle;
import java.util.UUID;

@Provider
public class ThrowableExceptionMapper implements ExceptionMapper<Throwable> {

    static Logger log = LoggerFactory.getLogger(ThrowableExceptionMapper.class);

    @Override
    public Response toResponse(Throwable e) {

        String errorId = UUID.randomUUID().toString();
        log.error("errorId[{}]", errorId, e);

        String defaultErrorMessage = ResourceBundle.getBundle("ValidationMessages").getString("System.error");

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).
                entity(new ErrorMessage(defaultErrorMessage + " - " + e.getMessage(), false)).build();
    }

}
