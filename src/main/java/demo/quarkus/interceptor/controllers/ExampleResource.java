package demo.quarkus.interceptor.controllers;

import demo.quarkus.interceptor.exceptionMapper.ExceptionUtility;
import demo.quarkus.interceptor.json.JsonRequestHello;
import demo.quarkus.interceptor.services.Service1;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import javax.validation.Valid;


@Path("/v1")
public class ExampleResource {

    @Inject
    private Service1 serviceHello;

    // 0 = NO exception
    public static final int CODE_LEVEL = 1;


    @GET
    @Path("/hello")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response helloGet(@QueryParam("exceptionLevel") int exceptionLevel) throws Exception {
        ExceptionUtility.isExceptionInThisCode(CODE_LEVEL, exceptionLevel);
        return this.serviceHello.getResponse(exceptionLevel, null);
    }


    @POST
    @Path("/hello")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response helloPost(@QueryParam("exceptionLevel") int exceptionLevel, @Valid JsonRequestHello jsonRequest) throws Exception{
        ExceptionUtility.isExceptionInThisCode(CODE_LEVEL, exceptionLevel);
        return this.serviceHello.getResponse(exceptionLevel, jsonRequest);
    }


    @GET
    @Path("/helloBis")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response helloGetBis() throws Exception {
        return this.serviceHello.getResponseBis(null);
    }


    @POST
    @Path("/helloBis")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response helloPostBis(@QueryParam("exceptionLevel") int exceptionLevel, @Valid JsonRequestHello jsonRequest) throws Exception{
       return this.serviceHello.getResponseBis(jsonRequest);
    }




}
