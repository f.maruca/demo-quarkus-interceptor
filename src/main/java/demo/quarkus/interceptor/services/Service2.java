package demo.quarkus.interceptor.services;

import demo.quarkus.interceptor.exceptionMapper.ExceptionUtility;
import demo.quarkus.interceptor.json.JsonRequestHello;
import demo.quarkus.interceptor.json.JsonResponseHello;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.core.Response;

@ApplicationScoped
public class Service2 {

    public static final int CODE_LEVEL = 3;


    public Response getResponse(int exceptionLevel, JsonRequestHello jsonRequest) throws Exception {
        ExceptionUtility.isExceptionInThisCode(CODE_LEVEL, exceptionLevel);
        JsonResponseHello jsonResponse = new JsonResponseHello();

        jsonResponse.setNome(jsonRequest==null ? "xxxxx" : jsonRequest.getNome());
        jsonResponse.setCognome("pluto");
        Response response = Response.status(Response.Status.OK).
                entity(jsonResponse).build();
        return response;
    }


    public Response getResponseBis(JsonRequestHello jsonRequest) throws Exception {
        throw new Exception("CustomException");
    }


}
