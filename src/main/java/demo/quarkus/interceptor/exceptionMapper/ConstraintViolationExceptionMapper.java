package demo.quarkus.interceptor.exceptionMapper;


import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.stream.Collectors;


@Provider
public class ConstraintViolationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {

    @Override
    public Response toResponse(ConstraintViolationException e) {
        List<ErrorMessage> errorMessages = e.getConstraintViolations().stream()
                .map(constraintViolation ->
                        new ErrorMessage(constraintViolation.getPropertyPath().toString() + " - " +  constraintViolation.getMessage(), false))
                .collect(Collectors.toList());
        return Response.status(Response.Status.BAD_REQUEST).entity(errorMessages).build();
    }


}
